let tourists = [];

tourists[0] = {
  name: "Mark",
  age: 19,
  cities: ["Tbilisi", "London", "Rome", "Berlin"],
  expenses: [120, 200, 150, 140],
};

tourists[1] = {
  name: "Bob",
  age: 21,
  cities: ["Miami", "Moscow", "Venice", "Riga", "Kyiv"],
  expenses: [90, 240, 100, 76, 123],
};

tourists[2] = {
  name: "Sam",
  age: 22,
  cities: ["Tbilisi", "Budapest", "Warsaw", "Vilnius"],
  expenses: [118, 95, 210, 236],
};

tourists[3] = {
  name: "Anna",
  age: 20,
  cities: ["New York", "Athens", "Sydney", "Tokyo"],
  expenses: [100, 240, 50, 190],
};

tourists[4] = {
  name: "Alex",
  age: 23,
  cities: ["Paris", "Tbilisi", "Madrid", "Marseille", "Minsk"],
  expenses: [96, 134, 76, 210, 158],
};

// Problem 2 - Is Tourist An Adult?

for (let i = 0; i < tourists.length; i++) {
  tourists[i].isAdult = tourists[i].age >= 21;
}

// Problem 3 Has He/She Visited Tbilisi?

for (let i = 0; i < tourists.length; i++) {
  tourists[i].visitedTbilisi = tourists[i].expenses.tbilisi > 0;
}

// Problem 4 Total Travel Expenses
 for (let i = 0; i < tourists.length; i++) {
   let sumExpenses = 0;
   for (let x = 0; x < tourists[i].expenses.length; x++) {
     sumExpenses = sumExpenses + tourists[i].expenses[x];
     tourists[i].totalExpenses = sumExpenses;
   }
 }


// Problem 5 Average Travel Expenses
for (let i = 0; i < tourists.length; i++) {
  let sumExpenses = 0;
  for (let x = 0; x < tourists[i].expenses.length; x++) {
    sumExpenses = sumExpenses + tourists[i].expenses[x];
    tourists[i].averageExpenses = sumExpenses / tourists[i].cities.length;
  }
}

//Problem 6 Tourist Who Spends The Most

let maxExpenses = tourists[0].sumExpenses
let maxSpender = tourists[0].name
for(let i = 0; i<tourists.length; i ++) {
  tourists[i].sumExpenses > maxExpenses ? maxExpenses = tourists[i].sumExpenses : maxSpender = tourists[i].name
}

console.log(tourists[0]);
console.log(tourists[1]);
console.log(tourists[2]);
console.log(tourists[3]);
console.log(tourists[4]);
console.log(`${maxSpender} has spent the most`);